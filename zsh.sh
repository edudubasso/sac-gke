#!/bin/bash
sudo apt install zsh git -y && \
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" && \
chsh -s /bin/zsh && \
sudo chsh -s /bin/zsh