import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagesService } from './images/images.service';

@NgModule({
  imports: [CommonModule],
  providers: [ImagesService]
})
export class AssetsModule {
  constructor(private imagesService: ImagesService) { }
}
