import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class ImagesService {
  constructor(
    private iconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.register('background_lighter');
    this.register('background_darker');
    this.register('sac_icon');
    this.register('sac_icon_sem_fundo');
    this.register('sac');
    this.register('sac_sem_fundo');
    this.register('sac_summary');
  }

  private register(
    name: string,
    namespace = 'assets',
    path = `/assets/images/${name}.svg`
  ): void {
    this.iconRegistry.addSvgIconInNamespace(
      namespace,
      name,
      this.domSanitizer.bypassSecurityTrustResourceUrl(path)
    );
  }
}
